<?php
require("../protected/classes/Util.php");

session_start();

$cashStatement = $_SESSION['__cashStatementPdf__'];
$stateDate = $_SESSION['__startDate__'];
$endDate = $_SESSION['__endDate__'];
$username = $_SESSION['__username__'];

//die(print_r($cashStatement));
?>


	
	<style>
			table{width:100%}
			

			#pageHeader, #pageFooter{
				text-align:center;
				//font-weight:bold;
				font-family:"Times New Roman", Times, serif;
				font-size: 14px;
				color:#999999;
			}
			#headerContent{text-align:center;}

			#pageTitle{
				font-size: 22px;
				font-weight: bold;
				color: #000000;
			}

			#companyName{
				font-size: 20px;
                color: #000000;
			}

			#companyRegno{
				font-size: 12px;
			}   color: #000000;

			.sign{
				font-size: 12px;
				color: #000000;
			}

			#companyAddress{
				padding-left: 180px;
				font-size:10px;
				color:#999999;
				padding-top: -10px;
			}

			#logo{
				text-align:center;
				font-size: 30px;
				font-weight: bold;
				color:#000000;
				font-family:Arial, Helvetica, sans-serif;
				background-image:url(background.png);
				background-repeat: no-repeat;
				background-position:center;
				height: 100px;
				padding-top: 80px;
			}

			.variable{
				font-family:Arial, Helvetica, sans-serif;
				color: #000000;
                font-size:50px;
				font-weight: bold;
				width:40%;
				padding-top: 80px;
			}


			.black{
				color: #000000;
				font-weight: bold;
				font-size:12px;
				font-family:Geneva, Arial, Helvetica, sans-serif;
			}
			.sample {
            	border-width: 1px;
            	border-spacing: 2px;
            	border-style: outset;
            	border-color: #000000;
            	background-color: white;
            }
            
              .tdsample{
             border: 1px solid #000;
			 
              }
			.thsample{
             border: 1px solid #000;
			 
              }

		</style>
		
</head>

			
<body>

	
	
	<div id="container">
		<img align="left" src="http://localhost:8080/aegis/logo" style="height:40px" />
		<p id="pageHeader">
			<span id="pageTitle"><?php echo $username; ?> </span>
			
		</p>
		<p id="headerContent">Valuation Statement<p>
		<p id="headerContent">Currency: ZAR<p>
		<p id="headerContent"><?php echo $stateDate .' through ' . $endDate; ?><p>
		
		<div class="wrapper">
			<table class="sample" border='1px'>
						<thead class="thsample">
						<tr >
							<!--<td>Journal #</td>-->
							<td>Transc. Date</td>
							<td>Value Date</td>
							<td>Description</td>
							<td>Debit</td>
							<td>Credit</td>
							<td>Balance</td>

						</thead>
						<tbody>

				
				<?php foreach($cashStatement as $key=>$value){
				   echo
                   '<tr>' .
				   '<td class="tdsample">' . date ("d-m-Y", strtotime($value->transactionDate)) . '</td>' .
                   '<td class="tdsample">' . date ("d-m-Y", strtotime($value->valueDate)).'</td>' .
				   '<td class="tdsample">' . $value->description . '</td>' .
                   '<td class="tdsample">' . Util::formatNumber($value->debitAmount). '</td>' .
                   '<td class="tdsample">' . Util::formatNumber($value->creditAmount) . '</td>' .
				   '<td class="tdsample">' . Util::formatNumber($value->balance).'</td>' .

                   '</tr>';
                    }

				    ?>
			</table>
        </div>
	

	<div>
	</body>
</html>





