<?php

include("../../mpdf60/mpdf.php");
 
$mpdf=new mPDF('','A4','','',15,15,16,16,9,9); 


$mpdf->mirrorMargins = 1;
$mpdf->SetDisplayMode('fullpage','two');
$mpdf->list_number_suffix = ')';

$mpdf->debug  = true;
 
$mpdf->WriteHTML(file_get_contents('ContractNote.html'));
         
$mpdf->Output();