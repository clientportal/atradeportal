<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/7/12
 * Time: 7:30 AM
 * To change this template use File | Settings | File Templates.
 */
Prado::using('System.Web.UI.ActiveControls.*');

class SecurityOverview extends TPage
{



    public function onInit($param)
    {
        parent::onInit($param);

        //Set the page title
        $this->Page->Title = "Client Portal - Security Overview ";

		$session = Prado::getApplication()->getSession();
        if (!$this->IsPostBack) // if the page is requested the first time
        {
           
			
			
            try {

                $webservice = new WebServiceClient(
                    Prado::getApplication()->Parameters['mcs-wsdl'],
                    Prado::getApplication()->Parameters['ws-username'],
                    Prado::getApplication()->Parameters['ws-password']);

                //Get the portfolio is not already bound
				$securities = $session['__all_securities__'];
				if ($securities == null) {
					$url = Prado::getApplication()->Parameters['market_data_url'] . 'rest/api/v1/research/get-security-list';
					if(@file_get_contents($url)){
					$var = file_get_contents($url);
					$result = json_decode($var, true);
					$result = $result['result'];

					//die(print_r(json_decode (json_encode ($result), FALSE)));

					$securities = json_decode (json_encode ($result), FALSE);
					$session['__all_securities__'] = $securities;
				}
			}
				
				

				if (isset($securities) && count($securities) > 0) {
					$this->securities->DataSource = $session['__all_securities__'];
					$this->securities->dataBind();

					$request = $this->getApplication()->getRequest();
					$symbol = $request['symbol'];

					if ($symbol != null) {

						$this->securities->SelectedValue = strtoupper($symbol);
						$this->renderSecurityData(strtoupper($symbol));
					} else {
						$this->securities->SelectedValue = $securities[0]->name;
						$this->renderSecurityData($securities[0]->name);
					}
				}

                //Prado::log(print_r($cust), TLogger::ERROR, 'AppException');
            } catch (SoapFault $e) {
                throw new AppException(500, 'Unable to process request - ' . $e->faultstring);
            }

        }

		
		$url = "http://mds.zanibal.com/mds/rest/api/v1/research/get-market-news?c=25&b=0";
		$marketNews = Util::getJSONfromURL($url);
		$session['__market_news__'] = $marketNews['result'];
		//die(print_r($session['__market_news__']));
		
		//die(print_r($session['__market_news__']));
		$this->MarketNewsRepeater->DataSource = $session['__market_news__'];
		$this->MarketNewsRepeater->dataBind();
		
		
    }

    private function renderSecurityData($symbol)
    {
        $session = Prado::getApplication()->getSession();
        $webservice = new WebServiceClient(
            Prado::getApplication()->Parameters['mcs-wsdl'],
            Prado::getApplication()->Parameters['ws-username'],
            Prado::getApplication()->Parameters['ws-password']);


        //Get the details
		//Get the details
		if ($symbol != null) {
			
			$url = Prado::getApplication()->Parameters['market_data_url'] . 'rest/api/v1/research/get-security-overview/symbol?x='  . Prado::getApplication()->Parameters['default-exchange'] . '&s=' . strtoupper($symbol);
			if(@file_get_contents($url)){
			$json = file_get_contents($url);
			$obj = json_decode($json);
			//$sd = $webservice->getWebService()->findSecurityOverviewById($secId);
			//die(print_r($obj));
			$session['__currentsecurity__'] = $obj;

			//this check if security is empty
			if(isset($obj->msgCode)){
				$session['__SymbolNotFound__'] = 'No record found for "'. $symbol . '."'; 
			}else{
				$session['__SymbolNotFound__'] ='';
			}
		}
		}
    }


    public function updateSecurityOverview($sender, $param)
    {
        $this->renderSecurityData($sender->SelectedValue);
    }
}

?>