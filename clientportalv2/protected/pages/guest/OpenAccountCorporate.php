<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/7/12
 * Time: 7:30 AM
 * To change this template use File | Settings | File Templates.
 */
Prado::using('System.Web.UI.ActiveControls.*');

class OpenAccountCorporate extends TPage
{


    public function onInit($param)
    {
        parent::onInit($param);

        $session = Prado::getApplication()->getSession();
        $session->open();


    }

    public function viewChanged($sender, $param)
    {

        $session = Prado::getApplication()->getSession();
        $session->open();

        $ob = $session['__customer_application__'];
        if ($ob == null) {
            $ob = new CustomerBean();
            $session['__customer_application__'] = $ob;
        }

        else  if ($this->OpenAccount->ActiveViewIndex === 0) {

          $ob->partnerType = "ORGANIZATION";

        } else  if ($this->OpenAccount->ActiveViewIndex === 1 && $this->IsPostBack) {
            $this->bindFormValues1($ob);
        } else  if ($this->OpenAccount->ActiveViewIndex === 2 && $this->IsPostBack) {
            $this->bindFormValues2($ob);
        } else  if ($this->OpenAccount->ActiveViewIndex === 3 && $this->IsPostBack) {
            //$this->createCustomer();
			
			$this->customAttachment();
        }
    }
	
				
				private function customAttachment()
				{
					
					$session = Prado::getApplication()->getSession();
					
					//Now we should also handle the custom files if they where uploaded.
					$cb = $session['__customer_application__'];
					
					$id = $this->createCustomer();

					 //Now we will fetch the customer from the backend so that we can have the ecrm ID
					 $webservice = new WebServiceClient(
					  Prado::getApplication()->Parameters['mcs-wsdl'],
					  Prado::getApplication()->Parameters['ws-username'],
					  Prado::getApplication()->Parameters['ws-password']);
					  $customer = $webservice->getWebService()->findCustomerById($id);
						
						$session['__customer_application_name__'] = $customer->name;					
 
					 //die(var_dump($customer->ecrmId));
					 //We will now upload the file if everything checks out.
					$commonDocWebService = new WebServiceClient(
						Prado::getApplication()->Parameters['cds-wsdl'],
						Prado::getApplication()->Parameters['ws-username'],
						Prado::getApplication()->Parameters['ws-password']);
					 
					 if($cb->file5LN != null ){
						 $note = array(
							"active" => true,
							"category" => "KYC",
							"fileMimeType" => $cb->file5FT,
							"fileName" => $cb->file5FN,
							"fileSize" => $cb->file5FS,
							"label" => "COMPANY REGISTRATION DOCUMENT " . $customer->label,
							"type" => "RECORD",
							"relatedToId" => $customer->ecrmId,
							"relatedToType" => "CUSTOMER",
							"base64Attachment" => $cb->file5LN);		
					$commonDocWebService->getWebService()->createDocumentNoteWS($note);
					 }
					 
					 if($cb->file6LN != null ){
						 $note = array(
							"active" => true,
							"category" => "KYC",
							"fileMimeType" => $cb->file6FT,
							"fileName" => $cb->file6FN,
							"fileSize" => $cb->file6FS,
							"label" => "NOTICE OF REGISTERED OFFICE " . $customer->label,
							"type" => "RECORD",
							"relatedToId" => $customer->ecrmId,
							"relatedToType" => "CUSTOMER",
							"base64Attachment" => $cb->file6LN);		
					$commonDocWebService->getWebService()->createDocumentNoteWS($note);
					 }
					 
					 if($cb->file7LN != null ){
						 $note = array(
							"active" => true,
							"category" => "KYC",
							"fileMimeType" => $cb->file7FT,
							"fileName" => $cb->file7FN,
							"fileSize" => $cb->file7FS,
							"label" => "RESOLUTION LETTER " . $customer->label,
							"type" => "RECORD",
							"relatedToId" => $customer->ecrmId,
							"relatedToType" => "CUSTOMER",
							"base64Attachment" => $cb->file7LN);		
					$commonDocWebService->getWebService()->createDocumentNoteWS($note);
					 }
					 
					 if($cb->file8LN != null ){
						 $note = array(
							"active" => true,
							"category" => "KYC",
							"fileMimeType" => $cb->file8FT,
							"fileName" => $cb->file8FN,
							"fileSize" => $cb->file8FS,
							"label" => "SARS DOCUMENT " . $customer->label,
							"type" => "RECORD",
							"relatedToId" => $customer->ecrmId,
							"relatedToType" => "CUSTOMER",
							"base64Attachment" => $cb->file8LN);		
					$commonDocWebService->getWebService()->createDocumentNoteWS($note);
					 }
					 
					  if($cb->file9LN != null ){
						 $note = array(
							"active" => true,
							"category" => "KYC",
							"fileMimeType" => $cb->file9FT,
							"fileName" => $cb->file9FN,
							"fileSize" => $cb->file9FS,
							"label" => "BANK STATEMENT " . $customer->label,
							"type" => "RECORD",
							"relatedToId" => $customer->ecrmId,
							"relatedToType" => "CUSTOMER",
							"base64Attachment" => $cb->file9LN);		
					$commonDocWebService->getWebService()->createDocumentNoteWS($note);
					 } 
					 
					 if($cb->file10LN != null ){
						 $note = array(
							"active" => true,
							"category" => "KYC",
							"fileMimeType" => $cb->file10FT,
							"fileName" => $cb->file10FN,
							"fileSize" => $cb->file10FS,
							"label" => "Mode OF OF IDENTIFICATION FOR AUTHORISED SIGNATORY " . $customer->label,
							"type" => "RECORD",
							"relatedToId" => $customer->ecrmId,
							"relatedToType" => "CUSTOMER",
							"base64Attachment" => $cb->file10LN);		
					$commonDocWebService->getWebService()->createDocumentNoteWS($note);
					 }
					 
					 if($cb->file11LN != null ){
						 $note = array(
							"active" => true,
							"category" => "KYC",
							"fileMimeType" => $cb->file11FT,
							"fileName" => $cb->file11FN,
							"fileSize" => $cb->file11FS,
							"label" => "PROOF OF ADDRESS FOR AUTHORISED SIGNATORY " . $customer->label,
							"type" => "RECORD",
							"relatedToId" => $customer->ecrmId,
							"relatedToType" => "CUSTOMER",
							"base64Attachment" => $cb->file11LN);		
					$commonDocWebService->getWebService()->createDocumentNoteWS($note);
					 }
							
							$session['__customer_application__'] = null;
						}




    private function buildSOAPDocument($cb)
    {

        $doc = array(
            "id" => NULL,
            "name" => NULL,
            "active" => true, //compulsory
            "allowDebitBalance" => false, //compulsory
            "customerGroupName" => Prado::getApplication()->Parameters['customer-group-code-corporate'], //compulsory
            "businessOfficeName" => Prado::getApplication()->Parameters['business-office-code'], //compulsory
            "customerType" => "REGULAR", //compulsory
            "channel" => "WEB", //compulsory

            "setttlementBankName" => $cb->setttlementBankName,
            "setttlementBankBranch" => $cb->setttlementBankBranch,
            "setttlementBankAccountSCode" => $cb->setttlementBankAccountSCode,
            "setttlementBankAccountName" => $cb->setttlementBankAccountName,
            "setttlementBankAccountNumber" => $cb->setttlementBankAccountNumber,
            "setttlementBankOpenDate" => $cb->setttlementBankOpenDate != null
                    ? Util::convertToJavaDate($cb->setttlementBankOpenDate) : NULL,

            "picture" => $cb->file1LN, //compulsory
            "identification" => $cb->file2LN, //compulsory
            "utilityBill" => $cb->file3LN, //compulsory
            "signature" => $cb->file4LN, //compulsory

            "pictureFileName" => $cb->file1FN, //compulsory
            "identificationFileName" => $cb->file2FN, //compulsory
            "utilityBillFileName" => $cb->file3FN, //compulsory
            "signatureFileName" => $cb->file4FN, //compulsory

            "pictureMimeType" => $cb->file1FN, //compulsory
            "identificationMimeType" => $cb->file2FN, //compulsory
            "utilityBillMimeType" => $cb->file3FN, //compulsory
            "signatureMimeType" => $cb->file4FN, //compulsory

            "portalUserName" => $cb->portalUserName, //compulsory
            "portalPassword" => $cb->portalPassword == null ? "Welcome123" : $cb->portalPassword, //compulsory
            "secretQuestion" => $cb->secretQuestion, //compulsory
            "secretAnswer" => $cb->secretAnswer, //compulsory

            "partnerType" => $cb->partnerType, //compulsory
            "motherMaidenName" => $cb->motherMaidenName, //compulsory
            "birthDate" => $cb->birthDate != null ? Util::convertToJavaDate($cb->birthDate) : NULL,

            "homePhone" => $cb->homePhone,
            "officePhone" => $cb->officePhone,
            "mobilePhone" => $cb->mobilePhone,
            "emailAddress1" => $cb->emailAddress1,

            "title" => $cb->title,
            "firstName" => $cb->firstName,
            "middleName" => $cb->middleName,
            "lastName" => $cb->lastName,

            "primaryAddress1" => $cb->primaryAddress1,
            "primaryAddress2" => $cb->primaryAddress2,
            "primaryCity" => $cb->primaryCity,
            "primaryState" => $cb->primaryState,
            "primaryPostCode" => $cb->primaryPostCode,
            "primaryCountry" => $cb->primaryCountry,

                //New parameters
                        "dWTExemptCd" => "824",
                        "dWTOverridePerc" => 0,
                        "iWTExemptCd" => "455",
                        "iWTOverridePerc" => 0,
                        "residentStatus" => "N",
                        "ficaCompliant" => "N",
                       "taxIdNumber" => "12345",
                       "countryOfDomicile" => "ZA",
                       "nationality" => "ZA",
                     //    "taxIdNumber" => $cb->taxIdNumber,
                     //    "countryOfDomicile" => $cb->primaryCountry,
                     //     "nationality" => $cb->nationality,


                      "identifierType" => "P",
                        "identifier" => $cb->identifier,
        );
        return $doc;
    }


    private function createCustomer()
    {
        $session = Prado::getApplication()->getSession();
        $ob = $session['__customer_application__'];

        //we will now create the order
        $doc = $this->buildSOAPDocument($ob);

        //die(print_r($doc));
        //Create the quote and track the ID
        $webservice = new WebServiceClient(
            Prado::getApplication()->Parameters['mcs-wsdl'],
            Prado::getApplication()->Parameters['ws-username'],
            Prado::getApplication()->Parameters['ws-password']);

        $id = $webservice->getWebService()->createCustomer($doc);
        $session['__customer_application__']->id = $id;
        $session['__customer_application_id__'] = $id;
			return $id;
    }


    public function bindFormValues1($ob)
    {
        $ob->portalUserName = $this->portalUserName->Text;
        $ob->portalPassword = $this->portalPassword->Text;
        $ob->secretQuestion = $this->secretQuestion->Text;

        $ob->secretAnswer = $this->secretAnswer->Text;
        $ob->primaryAddress1 = $this->primaryAddress1->Text;
        $ob->primaryAddress2 = $this->primaryAddress2->Text;
        $ob->primaryCity = $this->primaryCity->Text;
        $ob->primaryState = $this->primaryState->Text;
        $ob->primaryPostCode = $this->primaryPostCode->Text;
        $ob->primaryCountry = $this->primaryCountry->Text;
        //$ob->primaryCountry = $this->primaryCountry->Text;

        $ob->title = '';
        $ob->firstName = '';
        $ob->middleName = '';
        $ob->lastName = $this->lastName->Text;
        $ob->motherMaidenName = '';
        $ob->birthDate = $this->birthDate->Text;

        $ob->homePhone = $this->homePhone->Text;
        $ob->officePhone = $this->officePhone->Text;
        $ob->mobilePhone = $this->mobilePhone->Text;
        $ob->emailAddress1 = $this->emailAddress1->Text;

        $ob->identifierType = '';
        $ob->identifier = $this->identifier->Text;
        $ob->identifierExpDate = '';
    }

    public function bindFormValues2($ob)
    {
        $ob->setttlementBankName = $this->setttlementBankName->SelectedValue;
        $ob->setttlementBankAccountName = $this->setttlementBankAccountName->Text;
        $ob->setttlementBankAccountSCode = $this->setttlementBankAccountSCode->Text;
        $ob->setttlementBankAccountNumber = $this->setttlementBankAccountNumber->Text;
        $ob->setttlementBankBranch = $this->setttlementBankBranch->Text;
        $ob->setttlementBankOpenDate = $this->setttlementBankOpenDate->Text;
    }


    public function passport($sender, $param)
    {
        if ($sender->HasFile) {
            $this->Result1->Text = $sender->FileName;
            $session = Prado::getApplication()->getSession();
            $cb = $session['__customer_application__'];
            $cb->file1LN = Util::getEncodedFile($sender->LocalName);
            $cb->file1FT = $sender->FileType;
            $cb->file1FS = $sender->FileSize;
            $cb->file1FN = $sender->FileName;

        }
    }

    public function identifier($sender, $param)
    {
        if ($sender->HasFile) {
            $this->Result2->Text = $sender->FileName;
            $session = Prado::getApplication()->getSession();
            $cb = $session['__customer_application__'];
            $cb->file2LN = Util::getEncodedFile($sender->LocalName);
            $cb->file2FT = $sender->FileType;
            $cb->file2FS = $sender->FileSize;
            $cb->file2FN = $sender->FileName;

        }
    }

    public function utilitybill($sender, $param)
    {
        if ($sender->HasFile) {
            $this->Result3->Text = $sender->FileName;
            $session = Prado::getApplication()->getSession();
            $cb = $session['__customer_application__'];
            $cb->file3LN = Util::getEncodedFile($sender->LocalName);
            $cb->file3FT = $sender->FileType;
            $cb->file3FS = $sender->FileSize;
            $cb->file3FN = $sender->FileName;

        }
    }

    public function signature($sender, $param)
    {
        if ($sender->HasFile) {
            $this->Result4->Text = $sender->FileName;
            $session = Prado::getApplication()->getSession();
            $cb = $session['__customer_application__'];
            $cb->file4LN = Util::getEncodedFile($sender->LocalName);
            $cb->file4FT = $sender->FileType;
            $cb->file4FS = $sender->FileSize;
            $cb->file4FN = $sender->FileName;

        }
    }
	
    public function companyregistrationdocument($sender, $param)
    {
        if ($sender->HasFile) {
            $this->Result5->Text = $sender->FileName;
            $session = Prado::getApplication()->getSession();
            $cb = $session['__customer_application__'];
            $cb->file5LN = Util::getEncodedFile($sender->LocalName);
            $cb->file5FT = $sender->FileType;
            $cb->file5FS = $sender->FileSize;
            $cb->file5FN = $sender->FileName;

        }
    }
	
	public function noticeofregisteredoffice($sender, $param)
    {
        if ($sender->HasFile) {
            $this->Result6->Text = $sender->FileName;
            $session = Prado::getApplication()->getSession();
            $cb = $session['__customer_application__'];
            $cb->file6LN = Util::getEncodedFile($sender->LocalName);
            $cb->file6FT = $sender->FileType;
            $cb->file6FS = $sender->FileSize;
            $cb->file6FN = $sender->FileName;
        }
    }
	
	public function resolutionletter($sender, $param)
    {
        if ($sender->HasFile) {
            $this->Result7->Text = $sender->FileName;
            $session = Prado::getApplication()->getSession();
            $cb = $session['__customer_application__'];
            $cb->file7LN = Util::getEncodedFile($sender->LocalName);
            $cb->file7FT = $sender->FileType;
            $cb->file7FS = $sender->FileSize;
            $cb->file7FN = $sender->FileName;
        }
    }
	
	public function sars($sender, $param)
    {
        if ($sender->HasFile) {
            $this->Result8->Text = $sender->FileName;
            $session = Prado::getApplication()->getSession();
            $cb = $session['__customer_application__'];
            $cb->file8LN = Util::getEncodedFile($sender->LocalName);
            $cb->file8FT = $sender->FileType;
            $cb->file8FS = $sender->FileSize;
            $cb->file8FN = $sender->FileName;
        }
    }
	
	public function bankstatement($sender, $param)
    {
        if ($sender->HasFile) {
            $this->Result9->Text = $sender->FileName;
            $session = Prado::getApplication()->getSession();
            $cb = $session['__customer_application__'];
            $cb->file9LN = Util::getEncodedFile($sender->LocalName);
            $cb->file9FT = $sender->FileType;
            $cb->file9FS = $sender->FileSize;
            $cb->file9FN = $sender->FileName;
        }
    }
	
	public function signatoryidentifier($sender, $param)
    {
        if ($sender->HasFile) {
            $this->Result10->Text = $sender->FileName;
            $session = Prado::getApplication()->getSession();
            $cb = $session['__customer_application__'];
            $cb->file10LN = Util::getEncodedFile($sender->LocalName);
            $cb->file10FT = $sender->FileType;
            $cb->file10FS = $sender->FileSize;
            $cb->file10FN = $sender->FileName;
        }
    }
	
	public function signatoryproofofaddress($sender, $param)
    {
        if ($sender->HasFile) {
            $this->Result11->Text = $sender->FileName;
            $session = Prado::getApplication()->getSession();
            $cb = $session['__customer_application__'];
            $cb->file11LN = Util::getEncodedFile($sender->LocalName);
            $cb->file11FT = $sender->FileType;
            $cb->file11FS = $sender->FileSize;
            $cb->file11FN = $sender->FileName;
        }
    }


    public function validateUsername($sender, $param)
    {
        $webservice = new WebServiceClient(
            Prado::getApplication()->Parameters['mcs-wsdl'],
            Prado::getApplication()->Parameters['ws-username'],
            Prado::getApplication()->Parameters['ws-password']);

        $param->IsValid = false; //Default to false and then switch to true if we get a SOAP Fault
        try {
            $webservice->getWebService()->findCustomerByPortalUserName(strtolower($param->Value));
        } catch (SoapFault $e) {
            $param->IsValid = true;
        }
    }

    public function resetImage5($sender, $param)
    {
            $session = Prado::getApplication()->getSession();
           $this->Result5->Text = "";
           $session['__customer_application__']->file5FN = null ;
           $session['__customer_application__']->file5FT = null ;
           $session['__customer_application__']->file5FS = null ;
           $session['__customer_application__']->file5LN = null ;
    }

    public function resetImage6($sender, $param)
    {
            $session = Prado::getApplication()->getSession();
           $this->Result6->Text = "";
           $session['__customer_application__']->file6FN = null ;
           $session['__customer_application__']->file6FT = null ;
           $session['__customer_application__']->file6FS = null ;
           $session['__customer_application__']->file6LN = null ;
    }


    public function resetImage7($sender, $param)
    {
            $session = Prado::getApplication()->getSession();
           $this->Result7->Text = "";
           $session['__customer_application__']->file7FN = null ;
           $session['__customer_application__']->file7FT = null ;
           $session['__customer_application__']->file7FS = null ;
           $session['__customer_application__']->file7LN = null ;
    }

    public function resetImage8($sender, $param)
    {
            $session = Prado::getApplication()->getSession();
           $this->Result8->Text = "";
           $session['__customer_application__']->file8FN = null ;
           $session['__customer_application__']->file8FT = null ;
           $session['__customer_application__']->file8FS = null ;
           $session['__customer_application__']->file8LN = null ;
    }
	
	public function resetImage9($sender, $param)
    {
            $session = Prado::getApplication()->getSession();
           $this->Result9->Text = "";
           $session['__customer_application__']->file9FN = null ;
           $session['__customer_application__']->file9FT = null ;
           $session['__customer_application__']->file9FS = null ;
           $session['__customer_application__']->file9LN = null ;
    }
	
	public function resetImage10($sender, $param)
    {
            $session = Prado::getApplication()->getSession();
           $this->Result10->Text = "";
           $session['__customer_application__']->file10FN = null ;
           $session['__customer_application__']->file10FT = null ;
           $session['__customer_application__']->file10FS = null ;
           $session['__customer_application__']->file10LN = null ;
    }
	
	public function resetImage11($sender, $param)
    {
            $session = Prado::getApplication()->getSession();
           $this->Result11->Text = "";
           $session['__customer_application__']->file11FN = null ;
           $session['__customer_application__']->file11FT = null ;
           $session['__customer_application__']->file11FS = null ;
           $session['__customer_application__']->file11LN = null ;
    }

	public function validatePassword($sender, $param)
    {
        	//$param->IsValid = false;
		if($this->portalPassword->Text != $this->retypePortalPassword->Text ){
			$param->IsValid = false;
		}else{
			$param->IsValid = true;
		}
    }

}

?>
