<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/7/12
 * Time: 7:30 AM
 * To change this template use File | Settings | File Templates.
 */

Prado::using('System.Web.UI.ActiveControls.*');

class Home extends TPage
{

    /**
     * Set custom attributes on the page controls
     * @param $param
     * @return void
     */
    public function onInit($param)
    {
        parent::onInit($param);
        $session = Prado::getApplication()->getSession();
        $session->open();

        $this->Page->Title = "ClientPortal - ClientPortal Securities Limited";
		
	   $client = $session['__customer__'];


		
		$url = Prado::getApplication()->Parameters['market_data_url'] . 'rest/api/v1/research/get-market-movers?x='  . Prado::getApplication()->Parameters['default-exchange'] . '&c=20&t=MOST_ACTIVE';
		
		
		if( date('G:i') > $session['__login_failed_time__'] )
		{
			$session['__login_failed__'] = false;
		}

			

    }

		  
		 /**
     * Redirects the user's browser to appropriate URL if login succeeds.
     * This method responds to the login button's OnClick event.
     * @param mixed event sender
     * @param mixed event parameter
     */
	 
	 public function validateUser($sender, $param)
	{
		$authManager = $this->Application->getModule('auth');


		if (!$authManager->login($this->Username->Text, $this->Password->Text))
			$param->IsValid = false; // tell the validator that validation fails
		else {
			//Set the session length
			$authManager->setAuthExpire(600);

			//this set the cookies
			if($this->RememberMe->text == 'Remember me'){
				$cookie = new THttpCookie("user",Prado::getApplication()->getUser()->saveToString());
				$cookie->setExpire(3600);
				$this->Response->cookies->add($cookie);
				//die(var_dump($cookie));
			}


		}


	}
	 
    public function loginButtonClicked($sender, $param)
    {
        if ($this->Page->IsValid) // all validations succeed
        {
            // obtain the URL of the privileged page that the user wanted to visit originally
            $url = $this->Application->getModule('auth')->ReturnUrl;
            if (empty($url)) // the user accesses the login page directly
                //$url = $this->Service->DefaultPageUrl;
                $url = "index.php?page=client.CompleteViewByAccount";
            $this->Response->redirect($url);
        }
    }
	




}

?>