<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/7/12
 * Time: 7:30 AM
 * To change this template use File | Settings | File Templates.
 */

Prado::using('System.Web.UI.ActiveControls.*');

class PasswordReset extends TPage
{
    public function onInit($param)
    {
        parent::onInit($param);

        //Set the page title
        $this->Page->Title = "ClientPortalMTrader - Password Reset - " . $this->Application->Session['__customer__']->label;


        if (!$this->IsPostBack) // if the page is requested the first time
        {
            //$session = Prado::getApplication()->getSession();


        }
    }

    public function viewChanged($sender, $param)
    {
        $session = Prado::getApplication()->getSession();
        //$session->open();

       $client = $session['__customer__'];
       ;
        if ($this->PasswordReset->ActiveViewIndex == 0) {
            //do nothing
        } else  if ($this->PasswordReset->ActiveViewIndex === 1 && $this->IsPostBack) {

            //Get the old and new password

            $oldPassword = $this->OldPassword->Text;
            $newPassword = $this->NewPassword->Text;
            //todo veify match using the Prado framework

            //Call the backend and try to change the password
            $message = '';

             try {
                $this->resetPassword($client, $oldPassword, $newPassword);
                $message = 'Your password has been successfully changed';
            } catch (SoapFault $e) {
                 $message = 'Your password change failed';

            }

             $session['__password_reset_message__'] = $message;



        }
    }

    private function resetPassword($client, $oldPassword, $newPassword)
    {
        $webservice = new WebServiceClient(
            Prado::getApplication()->Parameters['mcs-wsdl'],
            Prado::getApplication()->Parameters['ws-username'],
            Prado::getApplication()->Parameters['ws-password']);

        //We will send in an empty password pending when the server is updated
        //$webservice->getWebService()->updateCustomerPassword($client->id, $oldPassword, $newPassword);
        $webservice->getWebService()->updateCustomerPassword($client->id, null, $newPassword);
    }
	
	public function serverValidate($sender,$param)
	{

		$session = Prado::getApplication()->getSession();
		if($param->Value != $session['__customer__']->portalPassword){
			return $param->IsValid = false;
		}else{
			return $param->IsValid = true;
		}


	}

}

?>