<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/7/12
 * Time: 7:30 AM
 * To change this template use File | Settings | File Templates.
 */

Prado::using('System.Web.UI.ActiveControls.*');

class MdsNewOrder extends TPage
{
	public function onInit($param)
	{
		parent::onInit($param);

		//Set the page title
		$this->Page->Title = "ARM Securities - New Equity Order - ";


		if (!$this->IsPostBack) // if the page is requested the first time
		{
			$session = Prado::getApplication()->getSession();
			try {

				$webservice = new WebServiceClient(
					Prado::getApplication()->Parameters['mcs-wsdl'],
					Prado::getApplication()->Parameters['ws-username'],
					Prado::getApplication()->Parameters['ws-password']);

				if(isset($session['__customer__'])){
					//Get the portfolio list
					$portfolios = $session['__portfolios__'];
					if ($portfolios == null) {
						$portfolios = $webservice->getWebService()->findCustomerPortfolios($session['__customer__']->id);
						$session['__portfolios__'] = $portfolios;
					}
					$this->Portfolio->DataSource = $session['__portfolios__']->item;
					$this->Portfolio->dataBind();

					//Get the instrument list
					//                $securities = $webservice->getWebService()->findActiveSecuritiesByType("NSE", "EQUITY", 0, 1000);
					//                $session['__securities__'] = $securities;

					if(isset($_GET['quantity'])){
						$this->Quantity->Text = $_GET['quantity'];
					}
					if(isset($_GET['limitPrice']) )
					{
						$this->LimitPrice->Text = $_GET['limitPrice']; 
						$this->PriceType->SelectedValue = 'LIMIT';
					}
					if(isset($_GET['orderType'])){
						$this->OrderType->SelectedValue = $_GET['orderType']; 
					}
					if(isset($_GET['price'])){
						$this->LimitPrice->Text = $_GET['limitPrice']; 
					}


					$url = Prado::getApplication()->Parameters['market_data_url'] . 'rest/api/v1/research/get-security-list?s=E';
					if(@file_get_contents($url)){
						$var = file_get_contents($url);
						$result = json_decode($var, true);
						$securities = json_decode (json_encode ($result['result']), FALSE);
						$session['__securities__'] =  $securities;


						$this->Security->DataSource = $session['__securities__'];
						$this->Security->dataBind();

						if(isset($_GET['symbol'])){
							$this->processSecurityUpdate($_GET['symbol']);
							$this->Security->SelectedValue= $_GET['symbol'];
						}elseif (isset($session['__securities__']) && count($session['__securities__']) > 0) {
							$this->processSecurityUpdate($session['__securities__'][0]->name);
						}else{}

					}

					//Get the order terms
					$orderterms = $session['__tradeorderterms__'];
					if ($orderterms == null) {
						$orderterms = $webservice->getWebService()->findActiveTradeOrderTerms();
						$session['__tradeorderterms__'] = $orderterms;
					}
					$this->OrderTerm->DataSource = $session['__tradeorderterms__']->item;
					$this->OrderTerm->dataBind();


					//Get the cash + overdraft
					$defcashbal = $webservice->getWebService()->findCustomerTradeBalanceById($session['__customer__']->id);
					$cashbal = $webservice->getWebService()->findCustomerTradeBalanceById($session['__customer__']->id);
					$creditbal = $webservice->getWebService()->findCustomerCreditLimitById($session['__customer__']->id);
					$session['__purchasingpower__'] = $defcashbal->amount + $creditbal->amount;
					$availbal = $webservice->getWebService()->findClearedCustomerTradeBalanceById($session['__customer__']->id);
					$session['__purchasingpower__'] = $availbal->amount + $creditbal->amount;
					$session['__cash_balance__'] = $cashbal->amount;


					//Always reset the order bean
					$ob = new OrderBean();
					$session['__orderbean__'] = $ob;

				}
				//Prado::log(print_r($cust), TLogger::ERROR, 'AppException');
			} catch (SoapFault $e) {
				throw new AppException(500, 'Unable to process request - ' . $e->faultstring);
			}

		}


	}

	public function processSecurityUpdate($selVal)
	{ //Update the security details
		$session = Prado::getApplication()->getSession();
		$webservice = new WebServiceClient(
			Prado::getApplication()->Parameters['mcs-wsdl'],
			Prado::getApplication()->Parameters['ws-username'],
			Prado::getApplication()->Parameters['ws-password']);


		$url = Prado::getApplication()->Parameters['market_data_url'] . 'rest/api/v1/research/get-security-overview/symbol?x='  . Prado::getApplication()->Parameters['default-exchange'] . '&s=' . strtoupper($selVal);

		if(@file_get_contents($url)){

			$sd = json_decode(file_get_contents($url));

			$session['__currentsecurity__'] = $sd;
			//			isset($sd->symbol) ? $this->CSymbol->Text = $sd->symbol : '' ;
			isset($sd->companyName) ? $this->CCompanyName->Text = $sd->companyName : '' ;
			isset($sd->sector) ? $this->CSector->Text = $sd->sector : '' ;
			isset($sd->lastTradePrice) ? $this->CLastPrice->Text = number_format($sd->lastTradePrice, 2, '.', ',') : '' ;
			isset($sd->priceChange) ? $this->CTodaysChange->Text = number_format($sd->priceChange, 2, '.', ',') : '' ;
			isset($sd->priceChangeP) ? $this->CTodaysChangeP->Text = number_format($sd->priceChangeP, 2, '.', ',') : '' ;
			isset($sd->highPrice) ? $this->CTodaysHigh->Text = number_format($sd->highPrice, 2, '.', ',') : '' ;
			isset($sd->lowPrice) ? $this->CTodaysLow->Text = number_format($sd->lowPrice, 2, '.', ',') : '' ;
			isset($sd->bestOfferQty) ? $this->CAskSize->Text = number_format($sd->bestOfferQty, 0, '.', ',') : '' ;
			isset($sd->bestOfferPrice) ? $this->CAskPrice->Text = number_format($sd->bestOfferPrice, 2, '.', ',') : '' ;
			isset($sd->bestBidQty) ? $this->CBidSize->Text = number_format($sd->bestBidQty, 0, '.', ',') : '' ;
			isset($sd->bestBidPrice) ? $this->CBidPrice->Text = number_format($sd->bestBidPrice, 2, '.', ',') : '' ;
			isset($sd->y52WeekHigh) ? $this->CY52WeekHigh->Text = number_format($sd->y52WeekHigh, 2, '.', ',') : '' ;
			isset($sd->y52WeekLow) ? $this->CY52WeekLow->Text = number_format($sd->y52WeekLow, 2, '.', ',') : '' ;

			if($sd->priceChangeP > 0){
				$marketIndicator ='<img src="themes/Default/design/img/arrup.png" />';
			}elseif($sd->priceChangeP < 0){
				$marketIndicator ='<img src="themes/Default/design/img/arrdown.png" />';
			}else{
				$marketIndicator ='<img src="themes/Default/design/img/center.png" />';
			}
			$this->CMarketIndicator->Text = $sd->marketIndicator = $marketIndicator;
		}

	}




	private function buildSOAPDocument($orderBean)
	{
		$doc = array(
			"id" => NULL,
			"portfolioName" => $orderBean->portfolioName, //compulsory
			"securityName" => $orderBean->securityName, //compulsory
			"limitPrice" => $orderBean->limitPrice, //compulsory
			"stopPrice" => $orderBean->stopPrice, //compulsory
			"priceType" => $orderBean->priceType, //compulsory
			"orderDate" => $orderBean->orderDate == null ? Util::convertToJavaDate(Util::getTodaysDate(null))
			: $orderBean->orderDate,
			"orderOrigin" => $orderBean->orderOrigin, //compulsory
			"orderType" => $orderBean->orderType, //compulsory
			"quantityRequested" => $orderBean->quantityRequested, //compulsory
			"orderCurrency" => $orderBean->orderCurrency == null ? "NGN" : $orderBean->orderCurrency, //compulsory
			"orderTermName" => $orderBean->orderTermName);
		return $doc;
	}

	public function bindFormValues($ob)
	{
		$ob->portfolioName = $this->Portfolio->SelectedValue;
		$ob->securityName = $this->Security->SelectedValue;
		$ob->limitPrice = $this->LimitPrice->Text;
		$ob->priceType = $this->PriceType->SelectedValue;
		$ob->orderType = $this->OrderType->SelectedValue;
		$ob->quantityRequested = $this->Quantity->Text;
		$ob->orderTermName = $this->OrderTerm->SelectedValue;
	}


	private function saveTradeOrder()
	{
		$session = Prado::getApplication()->getSession();
		$ob = $session['__orderbean__'];

		//we will now create the order
		$doc = $this->buildSOAPDocument($ob);


		//Create the quote and track the ID
		$webservice = new WebServiceClient(
			Prado::getApplication()->Parameters['mcs-wsdl'],
			Prado::getApplication()->Parameters['ws-username'],
			Prado::getApplication()->Parameters['ws-password']);

		$id = $webservice->getWebService()->createTradeOrder($doc);
		$session['__orderbean__'] = null;
		$session['__neworderid__'] = $id;
	}


	public function checkLimitPrice($sender, $param)
	{
		if ($this->PriceType->SelectedValue == "LIMIT" &&
			($this->LimitPrice->Text == null || $this->LimitPrice->Text == "")
		   )
			$param->IsValid = false;
		else
			$param->IsValid = true;
	}

	public function validateTradeOrder($sender, $param)
	{
		$session = Prado::getApplication()->getSession();
		$ob = $session['__orderbean__'];
		if ($ob == null) {
			$ob = new OrderBean();
			$session['__orderbean__'] = $ob;
		}

		$this->bindFormValues($ob);
		$doc = $this->buildSOAPDocument($ob);

		$webservice = new WebServiceClient(
			Prado::getApplication()->Parameters['mcs-wsdl'],
			Prado::getApplication()->Parameters['ws-username'],
			Prado::getApplication()->Parameters['ws-password']);

		//Will throw an EntityNotFound SOAP Fault if the there are validation errors
		$vr = true;
		try {
			//Validate and get the total
			$orderTotal = $webservice->getWebService()->validateTradeOrder($doc);

			//Reset the validation message
			$this->ValidationMsg->Text = "";
			$vr = true;

			//Update the order
			$session['__orderbean__']->orderCost = $orderTotal;
		} catch (SoapFault $e) {
			$vr = false;
			//process the exception
			if (Util::startsWith($e->faultstring, "INSUFFICIENT_FUNDS_FOR_BUY_ORDER")) {
				$tokens = explode("|", $e->faultstring);
				$tk1 = explode(">", $tokens[0]);
				$this->ValidationMsg->Text = "Estimated Order Total : " . $tk1[1];
			} else  if (Util::startsWith($e->faultstring, "INSUFFICIENT_SHARES_FOR_SELL_ORDER")) {
				$tokens = explode(">", $e->faultstring);
				$this->ValidationMsg->Text = "Available shares : " . $tokens[1];
			} else {
				$this->ValidationMsg->Text = $e->faultstring;
			}
		}

		$param->IsValid = $vr;
		return $vr;
	}



	public function viewChanged($sender, $param)
	{
		$session = Prado::getApplication()->getSession();
		//$session->open();

		$ob = $session['__orderbean__'];
		if ($ob == null) {
			$ob = new OrderBean();
			$session['__orderbean__'] = $ob;
		}

		if ($this->QuoteView->ActiveViewIndex == 0) {
			//do nothing
		} else  if ($this->QuoteView->ActiveViewIndex === 1 && $this->IsPostBack) {
			$this->bindFormValues($ob);

			//Get the labels for order terms and portfolio
			foreach ($session['__portfolios__']->item as $i) {
				if ($i->name == $ob->portfolioName) {
					$ob->portfolioLabel = $i->label;
					break;
				}
			}
			foreach ($session['__tradeorderterms__']->item as $i) {
				if ($i->name == $ob->orderTermName) {
					$ob->orderTermLabel = $i->label;
					break;
				}
			}
		} else  if ($this->QuoteView->ActiveViewIndex === 2 && $this->IsPostBack) {
			$this->saveTradeOrder();
		}
	}

}

?>
