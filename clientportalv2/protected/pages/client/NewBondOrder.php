<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/7/12
 * Time: 7:30 AM
 * To change this template use File | Settings | File Templates.
*/


Prado::using('System.Web.UI.ActiveControls.*');

class NewBondOrder extends TPage
{
    public function onInit($param)
    {
        parent::onInit($param);

        //Set the page title
        $this->Page->Title = "ClientPortal - New Bond Order - " . $this->Application->Session['__customer__']->label;


        if (!$this->IsPostBack) // if the page is requested the first time
        {
            $session = Prado::getApplication()->getSession();
            try {

                $webservice = new WebServiceClient(
                    Prado::getApplication()->Parameters['mcs-wsdl'],
                    Prado::getApplication()->Parameters['ws-username'],
                    Prado::getApplication()->Parameters['ws-password']);

                //Get the portfolio list
                $portfolios = $session['__portfolios__'];
                if ($portfolios == null) {
                    $portfolios = $webservice->getWebService()->findCustomerPortfolios($session['__customer__']->id);
                    $session['__portfolios__'] = $portfolios;
                }
                $this->Portfolio->DataSource = $session['__portfolios__']->item;
                $this->Portfolio->dataBind();

                //Get the instrument list
                $securities = $webservice->getWebService()->findActiveSecuritiesByType("NSE", "BOND_NAT", 0, 1000);
                $session['__bond_securities__'] = $securities;

                $this->Security->DataSource ? $this->Security->DataSource = $session['__bond_securities__']->item : '';
                $this->Security->dataBind();

                if ( count($this->Security->DataSource) > 0) {
                    $this->processSecurityUpdate($session['__bond_securities__']->item[0]->name);
                }

                //Get the order terms
                $orderterms = $session['__tradeorderterms__'];
                if ($orderterms == null) {
                    $orderterms = $webservice->getWebService()->findActiveTradeOrderTerms();
                    $session['__tradeorderterms__'] = $orderterms;
                }
                $this->OrderTerm->DataSource = $session['__tradeorderterms__']->item;
                $this->OrderTerm->dataBind();

                //Get the cash + overdraft
                $defcashbal = $webservice->getWebService()->findCustomerCurrentBalanceById($session['__customer__']->id);
                $creditbal = $webservice->getWebService()->findCustomerCreditLimitById($session['__customer__']->id);
                $session['__purchasingpower__'] = $defcashbal->amount + $creditbal->amount;

                //Always reset the order bean
                $ob = new OrderBean();
                $session['__orderbean__'] = $ob;

                //Prado::log(print_r($cust), TLogger::ERROR, 'AppException');
            } catch (SoapFault $e) {
                throw new AppException(500, 'Unable to process request - ' . $e->faultstring);
            }

        }


    }

    private function buildSOAPDocument($orderBean)
    {
        $doc = array(
            "id" => NULL,
            "portfolioName" => $orderBean->portfolioName, //compulsory
            "securityName" => $orderBean->securityName, //compulsory
            "limitPrice" => $orderBean->limitPrice, //compulsory
            "stopPrice" => $orderBean->stopPrice, //compulsory
            "priceType" => $orderBean->priceType, //compulsory
            "orderDate" => $orderBean->orderDate == null ? Util::convertToJavaDate(Util::getTodaysDate(null))
                    : $orderBean->orderDate,
            "orderOrigin" => $orderBean->orderOrigin, //compulsory
            "orderType" => $orderBean->orderType, //compulsory
            "quantityRequested" => $orderBean->quantityRequested, //compulsory
            "orderCurrency" => $orderBean->orderCurrency == null ? "NGN" : $orderBean->orderCurrency, //compulsory
            "orderTermName" => $orderBean->orderTermName);
        return $doc;
    }

    public function bindFormValues($ob)
    {
        $ob->portfolioName = $this->Portfolio->SelectedValue;
        $ob->securityName = $this->Security->SelectedValue;
        $ob->limitPrice = $this->LimitPrice->Text;
        $ob->priceType = $this->PriceType->SelectedValue;
        $ob->orderType = $this->OrderType->SelectedValue;
        $ob->quantityRequested = $this->Quantity->Text;
        $ob->orderTermName = $this->OrderTerm->SelectedValue;
    }


    private function saveTradeOrder()
    {
        $session = Prado::getApplication()->getSession();
        $ob = $session['__orderbean__'];

        //we will now create the order
        $doc = $this->buildSOAPDocument($ob);


        //Create the quote and track the ID
        $webservice = new WebServiceClient(
            Prado::getApplication()->Parameters['mcs-wsdl'],
            Prado::getApplication()->Parameters['ws-username'],
            Prado::getApplication()->Parameters['ws-password']);

        $id = $webservice->getWebService()->createTradeOrder($doc);
        $session['__orderbean__'] = null;
        $session['__neworderid__'] = $id;
    }


    public function checkLimitPrice($sender, $param)
    {
        if ($this->PriceType->SelectedValue == "LIMIT" &&
            ($this->LimitPrice->Text == null || $this->LimitPrice->Text == "")
        )
            $param->IsValid = false;
        else
            $param->IsValid = true;
    }

    public function validateTradeOrder($sender, $param)
    {
        $session = Prado::getApplication()->getSession();
        $ob = $session['__orderbean__'];
        if ($ob == null) {
            $ob = new OrderBean();
            $session['__orderbean__'] = $ob;
        }

        $this->bindFormValues($ob);
        $doc = $this->buildSOAPDocument($ob);

        $webservice = new WebServiceClient(
            Prado::getApplication()->Parameters['mcs-wsdl'],
            Prado::getApplication()->Parameters['ws-username'],
            Prado::getApplication()->Parameters['ws-password']);

        //Will throw an EntityNotFound SOAP Fault if the there are validation errors
        $vr = true;
        try {
            //Validate and get the total
            $orderTotal = $webservice->getWebService()->validateTradeOrder($doc);

            //Reset the validation message
            $this->ValidationMsg->Text = "";
            $vr = true;

            //Update the order
            $session['__orderbean__']->orderCost = $orderTotal;
        } catch (SoapFault $e) {
            $vr = false;
            //process the exception
            if (Util::startsWith($e->faultstring, "INSUFFICIENT_FUNDS_FOR_BUY_ORDER")) {
                $tokens = explode("|", $e->faultstring);
                $tk1 = explode(">", $tokens[0]);
                $this->ValidationMsg->Text = "Estimated Order Total : " . $tk1[1];
            } else  if (Util::startsWith($e->faultstring, "INSUFFICIENT_SHARES_FOR_SELL_ORDER")) {
                $tokens = explode(">", $e->faultstring);
                $this->ValidationMsg->Text = "Available shares : " . $tokens[1];
            } else {
                $this->ValidationMsg->Text = $e->faultstring;
            }
        }

        $param->IsValid = $vr;
        return $vr;
    }


    public function securitySelected($sender, $param)
    {
        $selVal = $sender->SelectedValue;
        $this->processSecurityUpdate($selVal);

    }

    public function processSecurityUpdate($selVal)
    { //Update the security details
        $session = Prado::getApplication()->getSession();
        $webservice = new WebServiceClient(
            Prado::getApplication()->Parameters['mcs-wsdl'],
            Prado::getApplication()->Parameters['ws-username'],
            Prado::getApplication()->Parameters['ws-password']);

        $sd = $webservice->getWebService()->findSecurityOverviewByCode(Prado::getApplication()->Parameters['default-exchange'],
                                                                       $selVal);

        $session['__currentsecurity__'] = $sd;

        $this->CSymbol->Text = $sd->symbol;
        $this->CCompanyName->Text = $sd->companyName;
        $this->CSector->Text = $sd->sector;
        $this->CLastPrice->Text = number_format($sd->lastPrice, 2, '.', ',');
        $this->CTodaysChange->Text = number_format($sd->todaysChange, 2, '.', ',');
        $this->CTodaysChangeP->Text = number_format($sd->todaysChangeP, 2, '.', ',');
        $this->CTodaysHigh->Text = number_format($sd->todaysHigh, 2, '.', ',');
        $this->CTodaysLow->Text = number_format($sd->todaysLow, 2, '.', ',');
        $this->CAskSize->Text = isset($sd->askSize) ? number_format($sd->askSize, 0, '.', ',') : "0.00";
        $this->CBidSize->Text = isset($sd->bidSize) ? number_format($sd->bidSize, 0, '.', ','): "0.00";
        $this->CY52WeekHigh->Text = number_format($sd->y52WeekHigh, 2, '.', ',');
        $this->CY52WeekLow->Text = number_format($sd->y52WeekLow, 2, '.', ',');
    }


    public function viewChanged($sender, $param)
    {
        $session = Prado::getApplication()->getSession();
        //$session->open();

        $ob = $session['__orderbean__'];
        if ($ob == null) {
            $ob = new OrderBean();
            $session['__orderbean__'] = $ob;
        }

        if ($this->QuoteView->ActiveViewIndex == 0) {
            //do nothing
        } else  if ($this->QuoteView->ActiveViewIndex === 1 && $this->IsPostBack) {
            $this->bindFormValues($ob);

            //Get the labels for order terms and portfolio
            foreach ($session['__portfolios__']->item as $i) {
                if ($i->name == $ob->portfolioName) {
                    $ob->portfolioLabel = $i->label;
                    break;
                }
            }
            foreach ($session['__tradeorderterms__']->item as $i) {
                if ($i->name == $ob->orderTermName) {
                    $ob->orderTermLabel = $i->label;
                    break;
                }
            }
        } else  if ($this->QuoteView->ActiveViewIndex === 2 && $this->IsPostBack) {
            $this->saveTradeOrder();
        }
    }

}

?>
