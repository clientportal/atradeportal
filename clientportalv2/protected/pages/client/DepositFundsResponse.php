<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/7/12
 * Time: 7:30 AM
 * To change this template use File | Settings | File Templates.
 */

class DepositFundsResponse extends TPage
{


    public function onInit($param)
    {
        parent::onInit($param);

        //Set the page title
        $this->Page->Title = "ClientPortal - Deposit Funds Result - " . $this->Application->Session['__customer__']->label;


        $session = Prado::getApplication()->getSession();

        if ($session['__deposit_doc__'] != null) // if the page is requested the first time
        {
            $request = $this->getApplication()->getRequest();

            //Get the reference
            $txnRef = $request['txnref'] == null ? $session['__deposit_doc__']->name : $request['txnref'];
            $txnRef = $request['txnref'] == null ? $session['__deposit_doc__']->name : $request['txnref'];

            $resp = $request['resp'];
            $desc = $request['desc'];

            //Now we will make the webservice call to get the transaction status
            $url = Prado::getApplication()->Parameters['payment-gateway-query-url'] . "?productid=" . Prado::getApplication()->Parameters['payment-gateway-product-id'] . "&transactionreference=" . $txnRef . "&amount=" . $session['__deposite_totalFee__'] * 100;
            $hash_key = Prado::getApplication()->Parameters['payment-gateway-product-id'] . $txnRef . Prado::getApplication()->Parameters['payment-gateway-key'];
            $hash = hash("sha512", $hash_key);

            $ctx = stream_context_create(array(
                                              'http' => array(
                                                  'method' => 'GET',
                                                  'header' => "Hash: " . $hash
                                              )
                                         )
            );
            $response_xml_data = file_get_contents($url, 0, $ctx);

            //die(print_r($response_xml_data));

            if ($response_xml_data) {

                $webservice = new WebServiceClient(
                        Prado::getApplication()->Parameters['mcs-wsdl'],
                        Prado::getApplication()->Parameters['ws-username'],
                        Prado::getApplication()->Parameters['ws-password']);


                $data = simplexml_load_string($response_xml_data);
                if ($data) {
                    $resp = (string)$data->ResponseCode;
                    if(isset($data->ResponseDescription) && strlen((string) $data->ResponseDescription) > 0) {
                       $desc = (string)$data->ResponseDescription;
                    }

                

                    //Check the response code and process based on the response
                    if ($resp == "00") {     //if the transaction is successful
					
					    //First update the status of the transaction
						$webservice->getWebService()->updatePartnerCashTransactionAndRef($session['__deposit_doc__']->id, $session['__deposit_doc__']->name);
						
                        try {
                            //Post the transaction
                            $webservice->getWebService()->postPartnerCashTransactionByName($txnRef);
                            $session['__deposit_success__'] = true;
                            $session['__deposit_success_ref__'] = $session['__deposit_doc__']->name;
                            $session['__deposit_doc__'] = null;

                        } catch (SoapFault $e) {
                            $session['__deposit_doc__'] = null;
                            throw new AppException(500, 'Unable to process request - ' . $e->faultstring);
                        }
                    

                       } else {
                        $session['__deposit_success__'] = false;
                        $session['__deposit_message__'] = $desc;
						$session['__deposit_failure_ref__'] = $session['__deposit_doc__']->name;
                        $session['__deposit_doc__'] = null;
                    }
                }
            }

        }

    }

}

?>