<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/7/12
 * Time: 7:30 AM
 * To change this template use File | Settings | File Templates.
 */

Prado::using('System.Web.UI.ActiveControls.*');

class Profile extends TPage
{
	public function onInit($param)
	{
		parent::onInit($param);

		//Set the page title
		$this->Page->Title = "ClientPortal - Profile Page - " . $this->Application->Session['__customer__']->label;


		if (!$this->IsPostBack) // if the page is requested the first time
		{
			//$session = Prado::getApplication()->getSession();
		}

		//this function delete all images on that folder
		Profile::emptyGeneratedPictures();
	}

	public function viewChanged($sender, $param)
	{
		$session = Prado::getApplication()->getSession();
		//$session->open();

		$client = $session['__customer__'];

		$webservice = new WebServiceClient(
			Prado::getApplication()->Parameters['mcs-wsdl'],
			Prado::getApplication()->Parameters['ws-username'],
			Prado::getApplication()->Parameters['ws-password']);

		$getImagesPortaldetails =  $webservice->getWebService()->findCustomerAndImagesByPortalUserName($client->portalUserName);


		$session['__customerImages__'] = isset($getImagesPortaldetails->picture) ? Profile::getUserPic($getImagesPortaldetails->picture) : 'themes/Default/design/img/user.png';
		//die($session['__customerImages__'] );


		if ($this->ProfilePage->ActiveViewIndex == 0) {
			//do nothing
		} else  if ($this->ProfilePage->ActiveViewIndex === 1 && $this->IsPostBack) {

			isset($this->Application->Session['__customer__']->nexofKin)
				? $this->nexofKin->Text = $this->Application->Session['__customer__']->nexofKin : "";
			isset($this->Application->Session['__customer__']->nexofKinEmailAddress)
				? $this->nexofKinEmailAddress->Text = $this->Application->Session['__customer__']->nexofKinEmailAddress
				: "";
			isset($this->Application->Session['__customer__']->nextofKinPhone)
				? $this->nextofKinPhone->Text = $this->Application->Session['__customer__']->nextofKinPhone : "";
			isset($this->Application->Session['__customer__']->nextofKinAddress)
				? $this->nextofKinAddress->Text = $this->Application->Session['__customer__']->nextofKinAddress
				: "";
			isset($this->Application->Session['__customer__']->nextofKinRelationship)
				? $this->nextofKinRelationship->Text = $this->Application->Session['__customer__']->nextofKinRelationship
				: "";
			isset($this->Application->Session['__customer__']->secondaryAddress1)
				? $this->secondaryAddress1->Text = $this->Application->Session['__customer__']->secondaryAddress1
				: "";
			isset($this->Application->Session['__customer__']->secondaryCity)
				? $this->secondaryCity->Text = $this->Application->Session['__customer__']->secondaryCity : "";
			isset($this->Application->Session['__customer__']->secondaryState)
				? $this->secondaryState->Text = $this->Application->Session['__customer__']->secondaryState : "";
			isset($this->Application->Session['__customer__']->secondaryPostCode)
				? $this->secondaryPostCode->Text = $this->Application->Session['__customer__']->secondaryPostCode
				: "";
			isset($this->Application->Session['__customer__']->secondaryCountry)
				? $this->secondaryCountry->Text = $this->Application->Session['__customer__']->secondaryCountry
				: "";

		} else {


			//api to perform the task
			$cust = $this->Application->Session['__customer__'];

			$webservice = new WebServiceClient(
				Prado::getApplication()->Parameters['mcs-wsdl'],
				Prado::getApplication()->Parameters['ws-username'],
				Prado::getApplication()->Parameters['ws-password']);

			//Transfer all the old values that the client cannot change to the array
			$doc1 = $this->buildSOAPDocument($cust);
			$doc2 = $this->buildSOAPUpdate();
			//print_r($doc2);
			//combine arrays
			$doc3 = $doc1 + $doc2;
			try {
				//Update the user on the server
				$webservice->getWebService()->updateCustomer($doc3);

				$message = 'Your profile has been successfully updated';
			} catch (SoapFault $e) {
				$message = 'Your password change failed';
			}

			$session['__profile_update_message__'] = $message;

		}
	}


	private function buildSOAPDocument($c)
	{
		$doc = array(
			"id" => $c->id,
			"name" => isset($c->name) ? $c->name : "",
			"prefix" => isset($c->prefix) ? $c->prefix : "",
			"salutation" => isset($c->salutation) ? $c->salutation : "",
			"suffix" => isset($c->suffix) ? $c->suffix : "",
			"title" => isset($c->title) ? $c->title : "",
			"firstName" => isset($c->firstName) ? $c->firstName : "",
			"lastName" => isset($c->lastName) ? $c->lastName : "",
			"middleName" => isset($c->middleName) ? $c->middleName : "",
			"nationality" => isset($c->nationality) ? $c->nationality : "",
			"sex" => isset($c->sex) ? $c->sex : "",
			"channel" => isset($c->channel) ? $c->channel : "",
			"cellPhone" => isset($c->cellPhone) ? $c->cellPhone : "",
			"emailAddress1" => isset($c->emailAddress1) ? $c->emailAddress1 : "",
			"homePhone" => isset($c->homePhone) ? $c->homePhone : "",
			"emailAddress2" => isset($c->emailAddress2) ? $c->emailAddress2 : "",
			"officePhone" => isset($c->officePhone) ? $c->officePhone : "",
			"profession" => isset($c->profession) ? $c->profession : "",
			"fax" => isset($c->fax) ? $c->fax : "",
			"otherPhone" => isset($c->otherPhone) ? $c->otherPhone : "",
			"birthDate" => isset($c->birthDate) ? $c->birthDate : "",

			"motherMaidenName" => isset($c->motherMaidenName) ? $c->motherMaidenName : "",
			"identifier" => isset($c->identifier) ? $c->identifier : "",
			"identifierType" => isset($c->identifierType) ? $c->identifierType : "",
			"identifierExpDate" => isset($c->identifierExpDate) ? $c->identifierExpDate : "",


			"primaryAddress1" => isset($c->primaryAddress1) ? $c->primaryAddress1 : "",
			"primaryAddress2" => isset($c->primaryAddress2) ? $c->primaryAddress2 : "",
			"primaryCity" => isset($c->primaryCity) ? $c->primaryCity : "",
			"primaryPostCode" => isset($c->primaryPostCode) ? $c->primaryPostCode : "",
			"primaryState" => isset($c->primaryState) ? $c->primaryState : "",
			"primaryCountry" => isset($c->primaryCountry) ? $c->primaryCountry : "",


			"portalUserName" => isset($c->portalUserName) ? $c->portalUserName : "",
			"portalPassword" => isset($c->portalPassword) ? $c->portalPassword : "",
			"customerType" => isset($c->customerType) ? $c->customerType : "",
			"partnerType" => isset($c->partnerType) ? $c->partnerType : "",


			"setttlementBankAccountName" => isset($c->setttlementBankAccountName) ? $c->setttlementBankAccountName : "",
			"setttlementBankBranch" => isset($c->setttlementBankBranch) ? $c->setttlementBankBranch : "",
			"setttlementBankAccountNumber" => isset($c->setttlementBankAccountNumber) ? $c->setttlementBankAccountNumber
			: "",
			"setttlementBankAccountSCode" => isset($c->setttlementBankAccountSCode) ? $c->setttlementBankAccountSCode
			: "",
			"setttlementBankCity" => isset($c->setttlementBankCity) ? $c->setttlementBankCity : "",
			"setttlementBankName" => isset($c->setttlementBankName) ? $c->setttlementBankName : "",
			"setttlementBankPostCode" => isset($c->setttlementBankPostCode) ? $c->setttlementBankPostCode : "",
			"setttlementBankPostCountry" => isset($c->setttlementBankPostCountry) ? $c->setttlementBankPostCountry : "",
			"setttlementBankPostState" => isset($c->setttlementBankPostState) ? $c->setttlementBankPostState : "",
			"setttlementBankStreet" => isset($c->setttlementBankStreet) ? $c->setttlementBankStreet : "",
			"setttlementBankOpenDate" => isset($c->setttlementBankOpenDate) ? $c->setttlementBankOpenDate : "",

			"portalPassword" => strlen($c->portalPassword) > 0 ? $c->portalPassword : "Welcome123", //supply
			"secretQuestion" => isset($c->secretQuestion) ? $c->secretQuestion : "",
			"secretAnswer" => isset($c->secretAnswer) ? $c->secretAnswer : "",
			"profession" => isset($c->profession) ? $c->profession : "",
			"alertsEmail" => isset($c->alertsEmail) ? $c->alertsEmail : "",

		);
		return $doc;
	}

	Private function buildSOAPUpdate()
	{
		$session = Prado::getApplication()->getSession();
		$session['__customer__']->nexofKin = $this->nexofKin->Text;
		$session['__customer__']->nexofKinEmailAddress = $this->nexofKinEmailAddress->Text;
		$session['__customer__']->nextofKinPhone = $this->nextofKinPhone->Text;
		$session['__customer__']->nextofKinAddress = $this->nextofKinAddress->Text;
		$session['__customer__']->nextofKinRelationship = $this->nextofKinRelationship->Text;
		$session['__customer__']->secondaryAddress1 = $this->secondaryAddress1->Text;
		$session['__customer__']->secondaryCity = $this->secondaryCity->Text;
		$session['__customer__']->secondaryState = $this->secondaryState->Text;
		$session['__customer__']->secondaryPostCode = $this->secondaryPostCode->Text;
		$session['__customer__']->secondaryCountry = $this->secondaryCountry->Text;

		$doc = array(
			//variable from the form
			"secondaryAddress1" => $session['__customer__']->secondaryAddress1,
			"secondaryCity" => $session['__customer__']->secondaryCity,
			"secondaryState" => $session['__customer__']->secondaryState,
			"secondaryPostCode" => $session['__customer__']->secondaryPostCode,
			"secondaryCountry" => $session['__customer__']->secondaryCountry,
			"nexofKin" => $session['__customer__']->nexofKin,
			"nexofKinEmailAddress" => $session['__customer__']->nexofKinEmailAddress,
			"nextofKinPhone" => $session['__customer__']->nextofKinPhone,
			"nextofKinAddress" => $session['__customer__']->nextofKinAddress,
			"nextofKinRelationship" => $session['__customer__']->nextofKinRelationship,
		);

		return $doc;
	}



	public static function getUserPic($encodedImg)
	{
		$characters = '6';
		$filename = Profile::getPicName($characters);
		$newFile = time() . $filename . '.jpg';
		$destFolder = 'themes/Default/encryPics/';
		$outputfile = $newFile;

		$ifp = fopen($destFolder . $outputfile, "wb");
		fwrite($ifp, base64_decode($encodedImg));
		fclose($ifp);
		$decPic = $destFolder . $outputfile;
		return $decPic;
	}

	public static function getPicName($characters)
	{
		/* list all possible characters, similar looking characters and vowels have been removed */
		$possible = '23456789bcdfghjkmnpqrstvwxyz';
		$code = '';
		$i = 0;
		while ($i < $characters) {
			$code .= substr($possible, mt_rand(0, strlen($possible) - 1), 1);
			$i++;
		}
		return $code;
	}

	public static function emptyGeneratedPictures(){
		$session = Prado::getApplication()->getSession();
		//$session['__customerImages__'];
		$files = glob('themes/Default/encryPics/*.jpg');
		foreach($files as $file) {
			if($file != $session['__customerImages__']){
				unlink($file);
			}
		}
	}
}

?>