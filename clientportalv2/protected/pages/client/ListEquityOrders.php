<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/7/12
 * Time: 7:30 AM
 * To change this template use File | Settings | File Templates.
 */
Prado::using('System.Web.UI.ActiveControls.*');
class ListEquityOrders extends TPage
{


    public function onInit($param)
    {
        parent::onInit($param);

        //Set the page title
        $this->Page->Title = "ClientPortal - Equity Order List - " . $this->Application->Session['__customer__']->label;


        if (!$this->IsPostBack) // if the page is requested the first time
        {
            $session = Prado::getApplication()->getSession();
            try {

                $td = Util::getTodaysDate(null);
                $sd = Util::addDaysToDate($td, -30);
                $ed = Util::addDaysToDate($td, 1);

                $this->StartDate->Text = $sd;
                $this->EndDate->Text = $ed;

                $this->OrderStatus->SelectedValue = "";
                $this->renderOrderList(null, $sd, $ed);


                //Prado::log(print_r($cust), TLogger::ERROR, 'AppException');
            } catch (SoapFault $e) {
                throw new AppException(500, 'Unable to process request - ' . $e->faultstring);
            }

        }
		
		
		
		


    }

    private function renderOrderList($status, $startDate, $endDate)
    {
        $session = Prado::getApplication()->getSession();

        $webservice = new WebServiceClient(
            Prado::getApplication()->Parameters['mcs-wsdl'],
            Prado::getApplication()->Parameters['ws-username'],
            Prado::getApplication()->Parameters['ws-password']);


        //Get the order list
        if($status == "ALL") $status = null;
        $orderList = $webservice->getWebService()->findCustomerOrdersBySecurityType($session['__customer__']->id,
                                                                                    0, 1000000, null, $status, 		     Util::convertToJavaDate($startDate),
        Util::convertToJavaDate($endDate), "EQUITY");
	//die(print_r($orderList));

        $ol = isset($orderList->item) ? $orderList->item : array();
       $this->OrderListRepeater->DataSource = $ol;
        $this->OrderListRepeater->dataBind();


    }


    public function updateTradeOrderListPage($sender, $param)
    {
        $sd = $this->StartDate->Text;
        $ed = $this->EndDate->Text;
        $status = $this->OrderStatus->SelectedValue;

        $this->renderOrderList($status, $sd, $ed);
    }
}

?>