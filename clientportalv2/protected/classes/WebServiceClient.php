<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/8/12
 * Time: 4:08 PM
 * To change this template use File | Settings | File Templates.
 */

class WebServiceClient
{

    var $wsdl;
    var $username;
    var $password;
    var $__client;


    public function __construct($wsdl, $username, $password)
    {
        $this->wsdl = $wsdl;
        $this->username = $username;
        $this->password = $password;
    }


    public function getWebService()
    {
        //todo change the WSDL cache mode for production - WSDL_CACHE_MEMORY | WSDL_CACHE_DISK | WSDL_CACHE_BOTH
        if ($this->__client != null) return $this->__client;

        $client = new SoapClient($this->wsdl,
            array("trace" => 1,
                 "exceptions" => true,
                 "login" => $this->username,
                 "password" => $this->password,
                 "features" => SOAP_SINGLE_ELEMENT_ARRAYS,
                 "cache_wsdl" => WSDL_CACHE_MEMORY
            )
        );

        $this->__client = $client;
        return $client;

    }

}

?>