<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/15/12
 * Time: 8:27 AM
 * To change this template use File | Settings | File Templates.
 */
class Util
{

    //date_default_timezone_set('America/Los_Angeles');
    //date_default_timezone_set("Africa/Lagos");

    public static $DEFAULT_DATE_FORMAT = "d/m/Y";
    public static $DEFAULT_DATE_TIME_FORMAT = "d/m/Y HH:mm:ss";
    public static $JAVA_DATE_FORMAT = "Y-m-d";

    /**
     * Converts the java date string into the passed in format. If the $format argument is null, it will use the default date format
     * @static
     * @param $date
     * @param $format
     * @return string
     */
    public static function formatJavaDate($date, $format)
    {
        if ($date == null) return "";
        if ($format == null) $format = self::$DEFAULT_DATE_FORMAT;

        $dateArr = explode('T', $date);
        $dp = $dateArr[0];
        return date($format, strtotime($dp)); //todo we need to set the default time format so that strtotime works
    }

    /**
     * This method takes a european date format d/m/yyyy and returns the java date format
     * @static
     * @param $date
     * @return null|string
     */
    public static function convertToJavaDate($date)
    {
        //<%= date('d/m/Y h:m:sa', $this->Application->Session['__customer__']->lastLoginDttm)
        //2012-05-06T23:00:00.000Z

        if ($date == null) return null;

        //We first replace '/' with '-' so that the european date format is assumed.
        $di = str_replace("/", "-", $date);


        $fd = date(self::$JAVA_DATE_FORMAT, strtotime($di)); //sd->18/07/2012 ed->14/01/2013
        return $fd . 'T00:00:00.000Z'; //todo support time formats
    }

    public static function formatNumber($number)
    {
        return number_format($number, 2, '.', ',');
    }

    public static function formatNumberNoSeps($number)
       {
           return number_format($number, 2, '.', '');
       }



    public static function getTodaysDate($fmt)
    {
        $format = $fmt == null ? self::$DEFAULT_DATE_FORMAT : $fmt;
        return date($format);
    }

    public static function getCurrentTime($fmt)
    {
        $format = $fmt == null ? self::$DEFAULT_DATE_TIME_FORMAT : $fmt;
        return date($format);
    }

    public static function addDaysToDate($date, $days)
    {
        // $newdate = strtotime ( '-3 week' , strtotime ( $date ) ) ;
        // $newdate = strtotime ( '-3 month' , strtotime ( $date ) ) ;
        // $newdate = strtotime ( '-3 year' , strtotime ( $date ) ) ;

        //We first replace '/' with '-' so that the european date format is assumed.
        $di = str_replace("/", "-", $date);

        $newdate = strtotime($days . ' day', strtotime($di));
        return date(self::$DEFAULT_DATE_FORMAT, $newdate);
    }

    public static function getDatabaseConnection()
    {
        $dsn = Prado::getApplication()->Parameters['database-dsn'];
        $username = Prado::getApplication()->Parameters['database-username'];
        $password = Prado::getApplication()->Parameters['database-password'];

        $connection = new TDbConnection($dsn, $username, $password);
        // call setAttribute() to pass in additional connection parameters
        // $connection->Persistent=true;  // use persistent connection

        return $connection;

    }

    public function SimplifyErrorMessage($message)
    {
            $original = array("_", ">");//remove ,
            $replace = array(" ", "-");//replace with space
            $message =  str_replace($original,$replace,$message);//string replace formate the process
        return ucfirst(strtolower($message));//make the first statment capital can convert other cap to small
    }


    public static function getEncodedFile($path)
    {
        if (file_exists($path)) {
            $file = file_get_contents($path);
            $base64data = base64_encode($file);
            return $base64data;
        } else return NULL;
    }

    public static function startsWith($haystack, $needle)
    {
        return substr($haystack, 0, strlen($needle)) === $needle;
    }
	
	public static function convertToNormalDate($date)
	{
		if ($date == null) return null;
		$di = str_replace("-", "/", $date);
		$fd = date(self::$JAVA_DATE_FORMAT, strtotime($di));
		return $fd;
	}

	
	public static function getJSONfromURL($url)
	{
		
//		$username = Prado::getApplication()->Parameters['ws-username'];
//		$password = Prado::getApplication()->Parameters['ws-password'];
		$username = 'root';
		$password = 'support4demo';
		

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_UNRESTRICTED_AUTH, 1);

		$result = curl_exec($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);

	 	$data = json_decode($result, true);
		
		return  $data;

	
	}

}

?>