<?php
/**
 * @throws AppException
 * This class deals with capturing data and managing the lead to quote process
 */
class FixedDeposit
{
    public $autoRollover;
    public $currency;
    public $currentRate;
    public $rolloverRule;
    public $customerId;
    public $customerexpectedMaturity;
    public $faceValue;
    public $instrumentTypeexpectedMaturity;
    public $portfolioexpectedMaturity;
    public $startDate;
    public $tenure;
    public $expectedInterest;
    public $expectedMaturity;
    public $expectedNetInterest;
    public $portfolioLabel;
    public $orderCost;
    public $canChangeTenure;
    public $defaultRate;
    public $name;
    public $accruedInterest;
    public $customerLabel;
    public $status;
    public $instrumentTypeLabe;
   



    public function setAutoRollover($autoRollover)
    {
        $this->autoRollover = $autoRollover;
    }

    public function getAutoRollover()
    {
        return $this->autoRollover;
    }




    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }
    public function getCurrency()
    {
        return $this->currency;
    }

	
    public function setCurrentRate($currentRate)
    {
        $this->currentRate = $currentRate;
    }
    public function getCurrentRate()
    {
        return $this->currentRate;
    }

    public function setRolloverRule($rolloverRule)
    {
        $this->rolloverRule = $rolloverRule;
    }

    public function getRolloverRule()
    {
        return $this->rolloverRule;
    }

    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    public function getCustomerId()
    {
        return $this->customerId;
    }

    public function setCustomerexpectedMaturity($customerexpectedMaturity)
    {
        $this->customerexpectedMaturity = $customerexpectedMaturity;
    }

    public function getCustomerexpectedMaturity()
    {
        return $this->customerexpectedMaturity;
    }

    public function setFaceValue($faceValue)
    {
        $this->faceValue = $faceValue;
    }

    public function getFaceValue()
    {
        return $this->faceValue;
    }

    public function setInstrumentTypeexpectedMaturity($instrumentTypeexpectedMaturity)
    {
        $this->instrumentTypeexpectedMaturity = $instrumentTypeexpectedMaturity;
    }

    public function getInstrumentTypeexpectedMaturity()
    {
        return $this->instrumentTypeexpectedMaturity;
    }

    public function setPortfolioexpectedMaturity($portfolioexpectedMaturity)
    {
        $this->portfolioexpectedMaturity = $portfolioexpectedMaturity;
    }

    public function getPortfolioexpectedMaturity()
    {
        return $this->portfolioexpectedMaturity;
    }

    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function setTenure($tenure)
    {
        $this->tenure = $tenure;
    }

    public function getTenure()
    {
        return $this->tenure;
    }

    public function setExpectedInterest($expectedInterest)
    {
        $this->expectedInterest = $expectedInterest;
    }

    public function getExpectedInterest()
    {
        return $this->expectedInterest;
    }

    public function setExpectedMaturity($expectedMaturity)
    {
        $this->expectedMaturity = $expectedMaturity;
    }

    public function getExpectedMaturity()
    {
        return $this->expectedMaturity;
    }

    public function setExpectedNetInterest($expectedNetInterest)
    {
        $this->expectedNetInterest = $expectedNetInterest;
    }

    public function getExpectedNetInterest()
    {
        return $this->expectedNetInterest;
    }

    public function setPortfolioLabel($portfolioLabel)
    {
        $this->portfolioLabel = $portfolioLabel;
    }

    public function getPortfolioLabel()
    {
        return $this->portfolioLabel;
    }

    public function setOrderCost($orderCost)
    {
        $this->orderCost = $orderCost;
    }

    public function getOrderCost()
    {
        return $this->orderCost;
    }
	
	public function setCanChangeTenure($canChangeTenure)
    {
        $this->canChangeTenure = $canChangeTenure;
    }

    public function getCanChangeTenure()
    {
        return $this->canChangeTenure;
    }
	
	public function setDefaultRate($defaultRate)
    {
        $this->defaultRate = $defaultRate;
    }

    public function getDefaultRate()
    {
        return $this->defaultRate;
    }
	
	public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
	
	public function setAccruedInterest($accruedInterest)
    {
        $this->accruedInterest = $accruedInterest;
    }

    public function getAccruedInterest()
    {
        return $this->accruedInterest;
    }
	
	public function setAccruedNetInterest($accruedNetInterest)
    {
        $this->accruedNetInterest = $accruedNetInterest;
    }

    public function getAccruedNetInterest()
    {
        return $this->accruedNetInterest;
    }
	
	public function setCustomerLabel($customerLabel)
    {
        $this->customerLabel = $customerLabel;
    }

    public function getCustomerLabel()
    {
        return $this->customerLabel;
    }
	
	public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getStatus()
    {
        return $this->status;
    }
	
	public function setInstrumentTypeLabe($instrumentTypeLabe)
    {
        $this->instrumentTypeLabe = $instrumentTypeLabe;
    }

    public function getInstrumentTypeLabe()
    {
        return $this->instrumentTypeLabe;
    }
	
}

?>