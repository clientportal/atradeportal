<?php
class ClientLayout extends TTemplateControl
{


            /**
             * Set custom attributes on the page controls
             * @param $param
             * @return void
             */
            public function onInit($param)
            {
                parent::onInit($param);
                $session = Prado::getApplication()->getSession();
				
			/*	if($session['__customer__']->termsAndCondAccepted == FALSE){
				$url = "index.php?page=client.TermsAndAgreements";
				$this->Response->redirect($url);
				}*/

            }

    /**
     * Validates whether the username and password are correct.
     * This method responds to the TCustomValidator's OnServerValidate event.
     * @param mixed event sender
     * @param mixed event parameter
     */
    public function validateUser($sender, $param)
    {
        $authManager = $this->Application->getModule('auth');
        if (!$authManager->login($this->Username->Text, $this->Password->Text))
            $param->IsValid = false; // tell the validator that validation fails
        else {
            //Set the session length
             $authManager->setAuthExpire(600);
        }


    }

    /**
     * Redirects the user's browser to appropriate URL if login succeeds.
     * This method responds to the login button's OnClick event.
     * @param mixed event sender
     * @param mixed event parameter
     */
    public function loginButtonClicked($sender, $param)
    {
		$session = Prado::getApplication()->getSession();
		
		
        if ($this->Page->IsValid) // all validations succeed
        {
			$session['__login_failed__'] = false;
			
            // obtain the URL of the privileged page that the user wanted to visit originally
            $url = $this->Application->getModule('auth')->ReturnUrl;
            if (empty($url)
            ) // the user accesses the login page directly
                //$url = $this->Service->DefaultPageUrl;
                $url = "index.php?page=client.CompleteViewByAccount";
            $this->Response->redirect($url);
        }else{
			
			$session['__login_failed__'] = true;
			$session['__login_failed_time__'] = date('G:i', strtotime('+1 minutes', strtotime(date('c'))));

		}
    }

    public function logoutButtonClicked($sender, $param)
    {
        $this->Application->getModule('auth')->logout();
        $url = $this->Service->constructUrl($this->Service->DefaultPage);
        $this->Response->redirect($url);
    }

    public function getStockDetails($sender, $param)
    {
        if ($this->Page->IsValid) // all validations succeed
        {
            $symbol = $this->Symbol->Text;
            $url = "index.php?page=guest.SecurityOverview&symbol=" . $symbol;
            $this->Response->redirect($url);
        }
    }


}

?>