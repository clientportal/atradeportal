<div class="container-fluid steps-to-trade">
    <div class="container">
        <div class="row">
            <h3>Start trading in just 3 easy steps</h3>

            <div class="col-md-4">
                <div class="text-center">
                    <img alt="" src="themes/Default/images/img/open-account.png"/>
                </div>
                <h4>Open an account</h4>

                <p>
                    Register on ClientPortal,
                    <span class="block">Before you begin, ensure you meet</span>
                    <span class="block">the KYC Requirements.</span>
                    <a href="index.php?page=guest.OpenAccount" class="block">Sign Up</a>
                </p>
            </div>
            <div class="col-md-4">
                <div class="text-center">
                    <a href="#"><img alt="" src="themes/Default/images/img/fund-account.png"/></a>
                </div>
                <a href="#"><h4>Fund your account</h4></a>

                <p>
                    Fund your account online or <br/>
                    via any of our receiving banks <br/>
                    <a href="#"  data-reveal-id="myModal">Click here</a> to pay online
                    <!-- <span class="block">receiving banks and receive</span>
                  e-payment confirmation. -->
                </p>
            </div>
            <div class="col-md-4">
                <div class="text-center">
                    <img alt="" src="themes/Default/images/img/start-trading.png"/>
                </div>
                <h4>Start Trading!</h4>

                <p>
                    <span class="block">Buy and sell securities.</span>
                    <span class="block">Manage all your transactions on </span>
                    Afrinvestor!
                </p>
            </div>
        </div>
    </div>
    <span class="progress-line-1 visible-desktop"></span>
    <span class="progress-line-2 visible-desktop"></span>
</div>